<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '468460863525722',
        'client_secret' => '10bbe2e3d0efacd81e5a4c8fe154fa15',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'yjZMljE9705tU5H5tzZdFyYZm',
        'client_secret' => '6kNZAhYSDwy3xbDBRvPUz7qVkBWOfcHtUkE3V9lpfOvxp77Mjw',
        'redirect' => 'http://127.0.0.1:8000/login/twitter/callback',
    ],

    'google' => [
        'client_id' => '134088073360-o9rl72ujgtga9amjl9m25ofmg4cuge7q.apps.googleusercontent.com',
        'client_secret' => 'getpQSxAQXwDquLn2j7PvUTp',
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],

];
